<?php

declare(strict_types=1);

namespace cosy\framework\abstracts;

use cosy\framework\traits\ServiceTrait;

/**
 * ClassName AbstractService
 * Description TODO
 * Author BTC
 * Date 2023/10/31 14:19
 **/
abstract class AbstractService
{
    use ServiceTrait;

    public $mapper;
}