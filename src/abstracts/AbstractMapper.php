<?php

declare(strict_types=1);

namespace cosy\framework\abstracts;

use cosy\framework\model\CosyModel;
use cosy\framework\traits\MapperTrait;

/**
 * ClassName AbstractMapper
 * Description TODO
 * Author BTC
 * Date 2023/10/31 14:19
 **/
abstract class AbstractMapper
{
    use MapperTrait;

    /**
     * @var CosyModel
     */
    public $model;

    abstract public function assignModel();

    public function __construct()
    {
        $this->assignModel();
    }
}