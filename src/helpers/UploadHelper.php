<?php

declare(strict_types=1);

namespace cosy\framework\helpers;

/**
 * ClassName UploadHelper
 * Description TODO
 * Author BTC
 * Date 2023/11/16 19:37
 **/
class UploadHelper
{
    /**
     * 切片合并缓存前缀
     */
    const PREFIX_MERGE_CACHE = 'upload-file-guid:';

    /**
     * 上传配置
     *
     * @var array
     */
    public $config = [];

    /**
     * 上传路径
     *
     * @var array
     */
    public $paths = [];

    /**
     * 默认取 $_FILE['file']
     *
     * @var string
     */
    public $uploadFileName = 'file';

    /**
     * 上传驱动
     *
     * @var
     */
    protected $drive = 'local';

    /**
     * 拿取需要的数据
     *
     * @var array
     */
    protected $filter = [
        'thumb',
        'drive',
        'chunks',
        'chunk',
        'guid',
        'image',
        'compress',
        'width',
        'height',
        'md5',
        'poster',
        'writeTable',
    ];

    /**
     * 上传文件基础信息
     *
     * @var array
     */
    protected $baseInfo = [
        'name' => '',
        'width' => '',
        'height' => '',
        'size' => 0,
        'extension' => 'jpg',
        'url' => '',
        'merge' => false,
        'guid' => '',
        'type' => 'image/jpeg',
    ];

    /**
     * 是否切片上传
     *
     * @var bool
     */
    protected $isCut = false;

    /**
     * @var DriveInterface
     */
    protected $uploadDrive;

    /**
     * @var \League\Flysystem\Filesystem
     */
    protected $filesystem;

    /**
     * UploadHelper constructor.
     * @param array $config
     * @param string $type 文件类型
     * @param bool $superaddition 追加写入
     * @throws \Exception
     */
    public function __construct(array $config, $type, $superaddition = false)
    {
        // 过滤数据
        $this->filter($config, $type);
        // 设置文件类型
        $this->type = $type;
        // 初始化上传地址
        $this->initPaths();
        // 判断是否切片上传
        if (isset($this->config['chunks']) && isset($this->config['guid'])) {
            $this->drive = 'local';
            $this->isCut = true;
        }

        $drive = $this->drive;
        $this->uploadDrive = Yii::$app->uploadDrive->$drive([
            'superaddition' => $superaddition
        ]);
        $this->filesystem = $this->uploadDrive->entity();
    }

    /**
     * 验证文件
     *
     * @throws NotFoundHttpException
     */
    public function verifyFile()
    {
        $file = UploadedFile::getInstanceByName($this->uploadFileName);

        if (!$file) {
            throw new NotFoundHttpException('找不到上传文件');
        }

        if ($file->getHasError()) {
            throw new NotFoundHttpException('上传失败，请检查文件');
        }

        $this->baseInfo['extension'] = $file->getExtension();
        $this->baseInfo['size'] = $file->size;

        empty($this->baseInfo['name']) && $this->baseInfo['name'] = $file->getBaseName();
        $this->baseInfo['url'] = $this->paths['relativePath'] . $this->baseInfo['name'] . '.' . $file->getExtension();

        unset($file);
        $this->verify();
    }

    /**
     * 验证文件大小及类型
     *
     * @throws NotFoundHttpException
     */
    protected function verify()
    {
        if ($this->baseInfo['size'] > $this->config['maxSize']) {
            throw new NotFoundHttpException('文件大小超出网站限制');
        }

        if (!empty($this->config['extensions']) && !in_array($this->baseInfo['extension'],
                $this->config['extensions'])) {
            throw new NotFoundHttpException('文件类型不允许');
        }

        // 存储本地进行安全校验
        if ($this->drive == Attachment::DRIVE_LOCAL) {
            if ($this->type == Attachment::UPLOAD_TYPE_FILES && in_array($this->baseInfo['extension'],
                    $this->config['blacklist'])) {
                throw new NotFoundHttpException('上传的文件类型不允许');
            }
        }
    }

    public function save($path,$data = false)
    {
        // 拦截 如果是切片上传就接管
        if ($this->isCut == true) {
            $this->cut();
            return;
        }

        // 判断如果文件存在就重命名文件名
        if ($this->filesystem->has($this->baseInfo['url'])) {
            $name = explode('_', $this->baseInfo['name']);
//            $this->baseInfo['name'] = $name[0] . '_' . time() . '_' . StringHelper::random(8);
            $this->baseInfo['name'] = $name[0];
//            $this->baseInfo['url'] = $this->paths['relativePath'] . $this->baseInfo['name'] . '.' . $this->baseInfo['extension'];
            $this->baseInfo['url'] = $path . $this->baseInfo['name'] . '.' . $this->baseInfo['extension'];
        }
        // 判断是否直接写入
        if (false === $data) {
            $file = UploadedFile::getInstanceByName($this->uploadFileName);

            if (!$file->getHasError()) {
                $stream = fopen($file->tempName, 'r+');
                $result = $this->filesystem->writeStream($this->baseInfo['url'], $stream);

                if (!$result) {
                    throw new NotFoundHttpException('文件写入失败');
                }

                if (is_resource($stream)) {
                    fclose($stream);
                }
            } else {
                throw new NotFoundHttpException('上传失败，可能文件太大了');
            }
        } else {
            $result = $this->filesystem->write($this->baseInfo['url'], $data);

            if (!$result) {
                throw new NotFoundHttpException('文件写入失败');
            }
        }

        // 本地的图片才可执行
        if ($this->type == 'images' && $this->drive == 'local') {
            // 图片水印
            $this->watermark();
            // 图片压缩
            $this->compress();
            // 创建缩略图
            $this->thumb();

            // 获取图片信息
            if (empty($this->baseInfo['width']) && empty($this->baseInfo['height']) && $this->filesystem->has($this->baseInfo['url'])) {
                $imgInfo = getimagesize(Yii::getAlias('@attachment') . '/' . $this->baseInfo['url']);
                $this->baseInfo['width'] = $imgInfo[0] ?? 0;
                $this->baseInfo['height'] = $imgInfo[1] ?? 0;
            }
        }

        return;
    }

    public function getBaseInfo($cate_id)
    {
        // 是否切片
        if ($this->isCut == true) {
            return $this->baseInfo;
        }

        // 处理上传的文件信息
        $this->baseInfo['type'] = $this->filesystem->getMimetype($this->baseInfo['url']);
        $this->baseInfo['size'] = $this->filesystem->getSize($this->baseInfo['url']);
        $path = $this->baseInfo['url'];
        // 获取上传路径
        $this->baseInfo = $this->uploadDrive->getUrl($this->baseInfo, $this->drive, $this->config['fullPath']);

        $data = [
            'cate_id' => $cate_id,
            'drive' => $this->drive,
            'upload_type' => $this->type,
            'specific_type' => $this->baseInfo['type'],
            'size' => $this->baseInfo['size'],
            'width' => $this->baseInfo['width'],
            'height' => $this->baseInfo['height'],
            'extension' => $this->baseInfo['extension'],
            'name' => $this->baseInfo['name'],
            'md5' => $this->config['md5'] ?? '',
            'base_url' => $this->baseInfo['url'],
            'path' => $path
        ];

        // 写入数据库
        if (!isset($this->config['writeTable'])) {
            $attachment_id = Yii::$app->services->attachment->create($data);
            $this->baseInfo['id'] = $attachment_id;
        } elseif (isset($this->config['writeTable']) && $this->config['writeTable'] == StatusEnum::ENABLED) {
            $attachment_id = Yii::$app->services->attachment->create($data);
            $this->baseInfo['id'] = $attachment_id;
        }

        $this->baseInfo['formatter_size'] = Yii::$app->formatter->asShortSize($this->baseInfo['size'], 2);
        $this->baseInfo['upload_type'] = self::formattingFileType($this->baseInfo['type'], $this->baseInfo['extension'], $this->type);

        return $this->baseInfo;
    }
}