<?php

declare(strict_types=1);

namespace cosy\framework\model;

use think\Model;

/**
 * ClassName CosyModel
 * Description TODO
 * Author BTC
 * Date 2023/11/3 17:52
 **/
class CosyModel extends Model
{
    /**
     * 隐藏的字段列表
     * @var string[]
     */
    protected $hidden = ['deleted_at'];

    /**
     * 状态
     */
    public const ENABLE = '0';
    public const DISABLE = '1';

    /**
     * 默认每页记录数
     */
    public const PAGE_SIZE = 15;

    protected static $instance = null;

    /**
     * 单例模式
     *
     * @param array $option
     * @return static 这里一定要这样写，不然不会提示
     */
    public static function getInstance(array $option = []): CosyModel {
        $class = get_called_class();
        if (isset(self::$instance[$class])) {
            return self::$instance[$class];
        }
        self::$instance[$class] = new $class($option);
        return self::$instance[$class];
    }

    /**
     * 设置主键的值
     * @param string | int $value
     */
    public function setPrimaryKeyValue($value): void
    {
        $this->{$this->primaryKey} = $value;
    }

    /**
     * @return string
     */
    public function getPrimaryKeyType(): string
    {
        return $this->keyType;
    }
}