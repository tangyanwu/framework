<?php

declare(strict_types=1);

namespace cosy\framework\command\curd;

use cosy\framework\command\BaseMake;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;

/**
 * ClassName Controller
 * Description TODO
 * Author BTC
 * Date 2023/11/3 19:35
 **/
class Controller extends BaseMake
{
    protected $type = 'controller';

    protected function configure()
    {
        $this->setName('cosy:controller')
            ->addArgument('name', Argument::REQUIRED, 'Please input your class name')
            ->addArgument('tableName', Argument::REQUIRED, 'Please input your table name')
            ->setDescription('创建一个controller控制器');
    }

    protected function execute(Input $input, Output $output)
    {
//        $name = trim($input->getArgument('name'));
////
//        $classname = $this->getClassName($name);
////
//        $pathname = $this->getPathName($classname);
//        $this->output->write($pathname);
        $this->executeBuild($input, $output);
    }

    // 填充模板
    public function buildClass($name)
    {
        $stub = $this->getStub();
        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');

        $class = str_replace($namespace . '\\', '', $name);

        if(config('route.controller_suffix') == true){
            $controllerName = $class.'Controller';
        }else{
            $controllerName = $class;
        }

        $namespaceDirName = substr($namespace, 0, strrpos($namespace, '\\'));

        $search = [
            '{%createTime%}',
            '{%validateNameSpace%}',
            '{%className%}',
            '{%lowerClassName%}',
            '{%namespace%}',
            '{%controllerName%}',
            '{%serviceNameSpace%}',
            '{%serviceName%}',
        ];

        $replace = [
            date('Y-m-d H:i'),
            $namespaceDirName.'\validate',
            $class,
            lcfirst($class),
            $namespace,
            $controllerName,
            $namespaceDirName . '\service',
            $class . 'Service'
        ];
        return str_replace($search, $replace, $stub);
    }

    /**
     * 获取模板
     * @return string
     */
    protected function getStub()
    {
        $path = __DIR__.DIRECTORY_SEPARATOR.'stubs'.DIRECTORY_SEPARATOR.$this->type.'.stub';
        return file_get_contents($path);
    }

    protected function getClassName(string $name): string
    {
        if (strpos($name, '\\') !== false) {
            return $name;
        }

        if (strpos($name, '@')) {
            [$app, $name] = explode('@', $name);
        } else {
            $app = '';
        }

        if (strpos($name, '/') !== false) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->getNamespace($app) . '\\' . $name;
    }

    protected function getPathName(string $name): string
    {
        $name = str_replace('app\\', '', $name);
        if(config('route.controller_suffix') == true){
            $name = $name.'Controller';
        }
        return $this->app->getBasePath() . ltrim(str_replace('\\', '/', $name), '/') . '.php';
    }
}