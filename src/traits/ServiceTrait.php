<?php

declare(strict_types=1);

namespace cosy\framework\traits;

use cosy\framework\model\CosyModel;
use think\model\Collection;
use think\Paginator;

trait ServiceTrait
{
    /**
     * 获取列表数据
     * @param array|null $params
     * @param bool $isScope
     * @return array
     */
    public function getList(array $params = []): Collection
    {
        return $this->mapper->getList($params);
    }

    /**
     * 获取列表数据（带分页）
     * @param array $params
     * @return Paginator
     */
    public function getPageList(array $params = []): Paginator
    {
        return $this->mapper->getPageList($params);
    }

    /**
     * 根据主键读取一条数据
     *
     * @param int $id
     * @return CosyModel
     */
    public function read(int $id): CosyModel
    {
        return $this->mapper->read($id);
    }

    /**
     * 按条件读取一行数据
     *
     * @param array $where
     * @return CosyModel
     */
    public function first(array $where): CosyModel
    {
        return $this->mapper->first($where);
    }

    /**
     * 新增/编辑数据
     *
     * @param array $data
     * @return int
     */
    public function save(array $data): bool
    {
        return $this->mapper->save($data);
    }

    /**
     * 批量新增/更新
     * @param array $data
     * @return Collection
     */
    public function saveAll(array $data): \think\model\Collection
    {
        return $this->mapper->saveAll($data);
    }

    public function update(array $data)
    {
        return $this->mapper->update($data);
    }

    public function delete(array $data)
    {
        return $this->mapper->delete($data);
    }

    /**
     * 从回收站读取一条数据
     *
     * @param int $id
     * @return CosyModel
     */
    public function readByRecycle(int $id): CosyModel
    {
        return $this->mapper->readByRecycle($id);
    }
}