<?php

namespace cosy\framework\traits;

use cosy\framework\helpers\UploadHelper;

trait FileTrait
{
    public function images()
    {
        $upload = new UploadHelper($this->request->post(), Attachment::UPLOAD_TYPE_IMAGES);
        $upload->verifyFile();
        $upload->save();

        return $upload->getBaseInfo();
    }

    public function actionVideos()
    {
        $upload = new UploadHelper($this->request->post(), Attachment::UPLOAD_TYPE_VIDEOS);
        $upload->verifyFile();
        $upload->save();

        return $upload->getBaseInfo();
    }

    public function actionVoices()
    {
        $upload = new UploadHelper($this->request->post(), Attachment::UPLOAD_TYPE_VOICES);
        $upload->verifyFile();

        $upload->save();
        return $upload->getBaseInfo();
    }

    public function actionFiles()
    {
        $upload = new UploadHelper($this->request->post(), Attachment::UPLOAD_TYPE_FILES);
        $upload->verifyFile();
        $upload->save();
        return $upload->getBaseInfo();
    }
}