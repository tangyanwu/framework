<?php

declare(strict_types=1);

namespace cosy\framework\view;

/**
 * ClassName Table
 * Description TODO
 * Author BTC
 * Date 2023/11/6 13:44
 **/
class Table
{
    public function view(): string
    {
        $this->build();

        return $this->getSurface()->view();
    }
}