<?php

declare(strict_types=1);

namespace cosy\framework\exceptions;

use stdClass;
use think\exception\HttpResponseException;
use think\Response;

/**
 * ClassName UnprocessableException
 * Description TODO
 * Author BTC
 * Date 2023/11/7 16:45
 **/
class UnprocessableException extends HttpResponseException
{
    public $status;

    public $result;

    public function __construct($msg, $result = '', $code = 422, $status = 200, $header = [])
    {
        $this->message = $msg;
        $this->code = $code;
        $this->status = $status;
        if (empty($result) || $result === '{-null-}' || $result === NUll){
            $result = new stdClass();
        }
        $this->result = $result;

        $response = Response::create([
            'code' => $code, 'msg' => $msg, 'result' => $result
        ], 'json')->code($status)->header($header);
        $this->response = $response;
    }
}