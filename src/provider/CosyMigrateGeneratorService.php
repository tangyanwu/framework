<?php

declare(strict_types=1);

namespace cosy\framework\provider;

use cosy\framework\command\curd\Controller;
use cosy\framework\command\curd\Mapper;
use cosy\framework\command\curd\Model;
use cosy\framework\command\curd\Services;
use cosy\framework\command\curd\Validate;
use think\Service;

/**
 * ClassName CosyMigrateGeneratorService
 * Description TODO
 * Author BTC
 * Date 2023/11/3 18:40
 **/
class CosyMigrateGeneratorService extends Service
{
    public function boot()
    {
        $this->commands([
            Model::class,
            Controller::class,
            Services::class,
            Mapper::class,
            Validate::class
        ]);
    }
}