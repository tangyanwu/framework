<?php

declare(strict_types=1);

namespace cosy\framework\enums;

/**
 * ClassName CodeEnum
 * Description TODO
 * Author BTC
 * Date 2023/11/7 17:00
 **/
class CodeEnum
{
    const NORMAL = 200;
    const TOKEN_EXPIRED = 401;
    const Error= 422;

    /**
     * @return array
     */
    public static function getMap(): array
    {
        return [
            self::NORMAL => '正常',
            self::TOKEN_EXPIRED => 'TOKEN过期、不存在',
            self::Error => '错误',
        ];
    }
}